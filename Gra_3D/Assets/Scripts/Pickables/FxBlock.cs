﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FxBlock : MonoBehaviour
{
    public GameObject UnopenedBlock;
    public GameObject OpenedBlock;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            LivesCounter.Lives += 1;
            OpenedBlock.SetActive(true);
            UnopenedBlock.SetActive(false);
            this.gameObject.SetActive(false);
        }
    }
}

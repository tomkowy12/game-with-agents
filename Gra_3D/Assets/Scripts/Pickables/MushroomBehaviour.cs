﻿using Assets.Scripts.Agents.Tree;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    public float GoingUpSpeed = 2.0f;
    public float GoingRightSpeed = 2.0f;
    public float RightBorder;
    public float LeftBorder;

    public GameObject PickupColider;
    public Transform Player;

    public int AmountOfElevatingFrames = 60;
    public bool GoingRight = true;

    private uint framesGoingUp = 0;
    private Rigidbody rb;
    private BoxCollider bc;
    private BoxCollider bc2;
    private readonly DecTreeEnemyMove decTreeEnemyMove = DecTreeEnemyMove.GetDecTree();

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        bc = GetComponent<BoxCollider>();
        bc2 = PickupColider.GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.parent = null;
        if(framesGoingUp < AmountOfElevatingFrames)
        {
            //bc2.enabled = false;
            rb.useGravity = false;
            this.transform.position += Vector3.up * GoingUpSpeed * Time.deltaTime;

        }
        else if(framesGoingUp == AmountOfElevatingFrames)
        {
            //bc2.enabled = true;
            rb.useGravity = true;
            bc.enabled = true;
        }
        else
        {
            // miejsce dla mnie - grzyb już jest u góry
            // TODO: ustawić GoingRight np. decTreeMushroom.decideDirection(distance,coins,Time.deltaTime)
            bool position_right = Player.position.x > transform.position.x;
            float distance = Mathf.Sqrt(Mathf.Pow(Player.position.x - transform.position.x, 2) + Mathf.Pow(Player.position.y - transform.position.y, 2));
            ClassificationData data = new ClassificationData(CoinsCounter.PointCount, position_right, distance);
            GoingRight = decTreeEnemyMove.DecideMove(data);

            if (GoingRight)
            {
                transform.Translate(Vector3.right * GoingRightSpeed * Time.deltaTime, Space.World);
            }
            else
            {
                transform.Translate(Vector3.left * GoingRightSpeed * Time.deltaTime, Space.World);
            }

            if(transform.position.x > RightBorder)
            {
                GoingRight = false;
            }
            if(transform.position.x < LeftBorder)
            {
                GoingRight = true;
            }
           // rb.AddForce(new Vector3(1f, 0f) * GoingRightSpeed * Time.deltaTime, ForceMode.Acceleration);
        }
        framesGoingUp++;
    }
}

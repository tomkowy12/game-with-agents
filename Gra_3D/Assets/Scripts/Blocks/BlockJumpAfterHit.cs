﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockJumpAfterHit : MonoBehaviour
{
    float XPos;
    float YPos;
    float ZPos;

    public GameObject Box;
    public float ReactionHeight = 0.3f;
    public float Delay = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        XPos = Box.transform.position.x;
        YPos = Box.transform.position.y;
        ZPos = Box.transform.position.z;
    }

    private IEnumerator OnTriggerEnter(Collider other)
    {
        transform.GetComponent<Collider>().isTrigger = false;
        if(other.gameObject.tag == "Player")
        {
            Box.transform.position = new Vector3(XPos, (float)(YPos + ReactionHeight), ZPos);

            yield return new WaitForSeconds(Delay);

            Box.transform.position = new Vector3(XPos, (YPos), ZPos);

            yield return new WaitForSeconds(Delay);
            transform.GetComponent<Collider>().isTrigger = true;
        }
    }
}

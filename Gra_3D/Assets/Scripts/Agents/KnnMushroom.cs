﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Drawing;
using Emgu.CV.Structure;
using Emgu.CV.ML;
using Emgu.CV;
using System;
using Assets.Scripts.Agents.Tree;
using System.IO;

public class KnnMushroom
{
    private static KnnMushroom _knnMushroominstance = null;
    private static long startTimestamp;
    private KNearest _mushromkNearest = new KNearest();
    private CoinsCounter coinsCounter;

    public static KnnMushroom GetKnnMushroom()
    {
        if (_knnMushroominstance == null)
        {
            _knnMushroominstance = new KnnMushroom();
            startTimestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
        }
        return _knnMushroominstance;
    }

    private KnnMushroom()
    {
        // pobierz dane i zbuduj model knn

        int K = 10;

        int rowNumber = 0;
        Matrix<float> trainData = new Matrix<float>(39, 3);
        Matrix<float> trainClasses = new Matrix<float>(39, 1);
        using (var rd = new StreamReader("E:\\Projekty\\C#\\game-with-agents\\training_data.csv"))
        {
            while (!rd.EndOfStream)
            {
                var splits = rd.ReadLine().Split(';');

                trainData[rowNumber, 0] = float.Parse(splits[0]);
                trainData[rowNumber, 1] = float.Parse(splits[1]);
                trainData[rowNumber, 2] = float.Parse(splits[2]);
                trainClasses[rowNumber, 0] = float.Parse(splits[3]);
                rowNumber++;   // to move to the next row
            }
        }

        //int trainSampleCount = 100;

        #region Generate the training data and classes

        //Matrix<float> trainData = new Matrix<float>(trainSampleCount, 2);

        /*Matrix<float> sample = new Matrix<float>(1, 2);*/

        /*Matrix<float> trainData1 = trainData.GetRows(0, rowNumber >> 1, 1);
        trainData1.SetRandNormal(new MCvScalar(200), new MCvScalar(50));
        Matrix<float> trainData2 = trainData.GetRows(rowNumber >> 1, rowNumber, 1);
        trainData2.SetRandNormal(new MCvScalar(300), new MCvScalar(50));*/

        /*Matrix<float> trainClasses1 = trainClasses.GetRows(0, rowNumber >> 1, 1);
        trainClasses1.SetValue(1);
        Matrix<float> trainClasses2 = trainClasses.GetRows(rowNumber >> 1, rowNumber, 1);
        trainClasses2.SetValue(2);*/

        #endregion

        /*Matrix<float> results, neighborResponses;
        results = new Matrix<float>(sample.Rows, 1);
        neighborResponses = new Matrix<float>(sample.Rows, K);*/

        _mushromkNearest.DefaultK = K;
        _mushromkNearest.IsClassifier = true;
        _mushromkNearest.Train(trainData, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, trainClasses);
        int a = 2;
        a = 12;
        // estimates the response and get the neighbors' labels
        // prepare sample (now it is null) and move to decide()
        //float response = _mushromkNearest.Predict(sample); //knn.FindNearest(sample, K, results, null, neighborResponses, null);
    }

    public bool decide(ClassificationData data)
    {
        Matrix <float> data_prepared = new Matrix<float>(1,3);
        data_prepared[0, 0] = data.Distance; // distance
        data_prepared[0, 1] = Convert.ToSingle(data.Position); // position
        data_prepared[0, 2] = data.Points; // points
        bool dec = _mushromkNearest.Predict(data_prepared) != 0;
        Debug.Log("Decision tree agent decision: " + (dec ? "right" : "left"));
        return dec;
        //return true;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Agents.Tree
{
    public class DecisionResult : Decision
    {
        public bool Result
        {
            get; set;
        }
        public override bool Evaluate(ClassificationData client)
        {
            //Console.WriteLine("\r\nOFFER A LOAN: {0}", Result ? "YES" : "NO");
            return Result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Agents.Tree
{
    public class ClassificationData
    {
        private int points;
        public int Points
        {
            get
            {
                return points;
            }
            set
            {
                if (-1 < value && value < 4)
                {
                    points = value;
                }
            }
        }
        private bool position;
        public bool Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }
        private float distance;
        public float Distance
        {
            get
            {
                return distance;
            }
            set
            {
                distance = value;
            }
        }
        private bool knnDecision;
        public bool KNNDecision
        {
            get
            {
                return knnDecision;
            }
            set
            {
                knnDecision = value;
            }
        }

        public ClassificationData(int points, bool position, float distance)
        {
            this.points = points;
            this.position = position;
            this.distance = distance;
        }
    }
}

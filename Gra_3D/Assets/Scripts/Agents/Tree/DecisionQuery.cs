﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Agents.Tree
{
    public class DecisionQuery : Decision
    {
        public string Title
        {
            get; set;
        }
        public Decision Positive
        {
            get; set;
        }
        public Decision Negative
        {
            get; set;
        }
        public Func<ClassificationData, bool> Test
        {
            get; set;
        }

        public override bool Evaluate(ClassificationData client)
        {
            bool result = Test(client);
            //string resultAsString = result ? "right" : "left";

            //Console.WriteLine($"\t- {this.Title}? {resultAsString}");

            if (result) 
                return Positive.Evaluate(client);
            else 
                return Negative.Evaluate(client);

            //return result;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactiveAgent
{
    //TODO: dodać obiekt użytkownika i obiekt PiEnemyMove
    private static ReactiveAgent _reactiveAgentInstance = null;

    public static ReactiveAgent GetReactiveAgent()
    {
        if (_reactiveAgentInstance == null)
        {
            _reactiveAgentInstance = new ReactiveAgent();
        }
        return _reactiveAgentInstance;
    }

    private ReactiveAgent()
    {
    
    }
    
}

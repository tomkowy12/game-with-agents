﻿using Assets.Scripts.Agents.Tree;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DecTreeEnemyMove
{
    // TODO: prywatne pole drzewa decyzyjnego
    private ReactiveAgent reactiveAgent;
    private KnnMushroom knnMushroom = KnnMushroom.GetKnnMushroom();
    private static DecTreeEnemyMove decTreeEnemyMoveInstance = null;

    public static DecTreeEnemyMove GetDecTree()
    {
        if (decTreeEnemyMoveInstance == null)
        {
            decTreeEnemyMoveInstance = new DecTreeEnemyMove();
        }
        return decTreeEnemyMoveInstance;
    }
    private DecTreeEnemyMove()
    {
        // TODO: drzewo jest już określone, więc chyba nothing to do here

    }

    // TODO: metoda DecideMove ma używać stworzonego drzewa decyzyjnego i zwrócić odpowiedź prawo/lewo
    // ma brać pod uwagę reactive(underground)Agent - ruch: if (isUserUnderGround & y < -20) or (!isUnderground & y > -20)

    public bool DecideMove(ClassificationData data)
    {
        data.KNNDecision = knnMushroom.decide(data);
        DecisionQuery decision = MainDecisionTree();
        bool dec = decision.Evaluate(data);
        Debug.Log("Decision tree agent decision: " + (dec ? "right" : "left"));
        return dec;
    }


    private static DecisionQuery MainDecisionTree()
    {
        //Decision 3
        var distance = new DecisionQuery
        {
            Title = "Is user far from mushroom",
            Test = (data) => data.Distance > 4,
            Positive = new DecisionResult { Result = false },
            Negative = new DecisionResult { Result = true }
        };

        //Decision 2
        var knn = new DecisionQuery
        {
            Title = "Result of knn",
            Test = (data) => data.KNNDecision,
            Positive = new DecisionResult { Result = true },
            Negative = new DecisionResult { Result = false }
        };

        //Decision 1
        var position = new DecisionQuery
        {
            Title = "Is on the right side of mushroom",
            Test = (data) => data.Position,
            Positive = knn,
            Negative = distance
        };

        //Decision 0
        var points = new DecisionQuery
        {
            Title = "Have points",
            Test = (data) => data.Points > 1,
            Positive =  position,
            Negative = new DecisionResult { Result = false }
        };

        return points;
    }
}

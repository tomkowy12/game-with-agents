﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastleEntry : MonoBehaviour
{
    public GameObject FadeScreen;
    public GameObject Player;
    public AudioSource LevelEndSound;
    public int NextLevelNumber;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            StartCoroutine(FadeScreenAndGoNextLevel());
        }
    }

    private IEnumerator FadeScreenAndGoNextLevel()
    {
        LevelEndSound.Play();
        FadeScreen.SetActive(true);
        FadeScreen.GetComponent<Animator>().enabled = true;
        yield return new WaitForSeconds(0.495f);
        FadeScreen.GetComponent<Animator>().enabled = false;
        Application.LoadLevel(NextLevelNumber);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtons : MonoBehaviour
{
    public void PlayGame()
    {
        CoinsCounter.PointCount = 0;
        LivesCounter.Lives = 3;
        Application.LoadLevel(1);

    }
    public void QuitGame()
    {
        Application.Quit();

    }
}

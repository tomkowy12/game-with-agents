﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death_Level_1 : DeathScriptBase
{
    protected void OnTriggerEnter(Collider other)
    {
        
        base.OnTriggerEnter(other);
        CoinsCounter.PointCount = 0;
    }
    
    protected override IEnumerator DeathCoroutine()
    {
        yield return new WaitForSeconds(1);
        Application.LoadLevel(1);
    }
}

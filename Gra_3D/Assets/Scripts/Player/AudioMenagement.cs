﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioMenagement : MonoBehaviour
{
    public AudioSource DeathSound;
    public AudioSource MainLevelTheme;
    // Start is called before the first frame update
    void Start()
    {
        MainLevelTheme.Play();
    }

    public void PlayDeathSound() => DeathSound.Play();
    
    internal void StopPlayingTheme() => MainLevelTheme.Stop();
    
}

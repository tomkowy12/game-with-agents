﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{ 
    public Transform Player;
    public Vector3 Distance;

    // Update is called once per frame
    public void Update()
    {

        transform.position = Player.position + Distance;
    }

}

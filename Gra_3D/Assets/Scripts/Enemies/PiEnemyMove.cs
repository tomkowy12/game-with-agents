﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiEnemyMove : MonoBehaviour
{
    public float LeftBorder;
    public float RightBorder;
    public string Direction = "Left";
    public bool KilledPlayer = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Uruchom agenta i zdecyduj, czy Direction == "Left" czy "Right", czy nawet "Hold"
        // np. DecTreeEnemy.decideMove()
        if (!KilledPlayer)
        {
            if (Direction == "Left")
            {
                transform.Translate(Vector3.right * -2 * Time.deltaTime, Space.World);
                if (transform.position.x <= LeftBorder)
                {
                    Direction = "Right";
                }
            }
            else if (Direction == "Right")
            {
                transform.Translate(Vector3.right * 2 * Time.deltaTime, Space.World);
                if (transform.position.x >= RightBorder)
                {
                    Direction = "Left";
                }
            }
        }
    }
}

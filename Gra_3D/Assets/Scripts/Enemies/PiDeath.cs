﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiDeath : MonoBehaviour
{
    public GameObject PiEnemy;
    public GameObject DeathCollider_1;
    public GameObject DeathCollider_2;

    private IEnumerator OnTriggerEnter(Collider other)
    {
        this.GetComponent<BoxCollider>().enabled = false;
        PiEnemy.GetComponent<PiEnemyMove>().enabled = false;
        DeathCollider_1.SetActive(false);
        DeathCollider_2.SetActive(false);
        PiEnemy.transform.localScale -= new Vector3(0, 0.2f, 0.4f);
        PiEnemy.transform.localPosition -= new Vector3(0, 0, 0.2f);
        yield return new WaitForSeconds(1);
        PiEnemy.transform.position = new Vector3(0, -1000, 0);
    }
}

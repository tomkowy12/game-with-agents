﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level01_Load : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitingCoroutine());
        SceneManager.LoadScene(2);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator WaitingCoroutine()
    {
        yield return new WaitForSeconds(3f);
    }

}
